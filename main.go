package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("vim-go")
	http.Handle("/", http.HandlerFunc(info))
	err := http.ListenAndServe(":3333", nil)
	if err != nil {
		log.Fatal("Something went wrong: ", err)
	}
}

func info(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(about))
}

var about = `
{
	"name": "jimmy cozza",
	"email": "jimmy@cozza.online",
	"github": "jimmycozza",
	"github_url": "https://github.com/jimmycozza",
	"experience": [
		{
			"company": "solo",
			"position": "lead dev",
			"started": 2018
		},
		{
			"company": "grow",
			"position": "backend developer",
			"started": 2016
		},
		{
			"company": "bolste",
			"position": "backend developer",
			"started": 2015
		}
	],
	"skills": [
		{
			"parent": "javascript",
			"children": [
				"node",
				"react",
				"vuejs",
				"expressjs"
			]
		},
		{
			"parent": "golang",
			"children": []
		},
		{
			"parent": "other",
			"children": [
				"docker",
				"k8s",
				"restful design",
				"graphql"
			]
		}
	],
	"interests": [
		{
			"title": "piloting"
		},
		{
			"title": "hiking"
		},
		{
			"title": "baking"
		},
		{
			"title": "reading"
		}
	]
}
`
